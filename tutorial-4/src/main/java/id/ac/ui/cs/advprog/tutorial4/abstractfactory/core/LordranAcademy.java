package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

public class LordranAcademy extends KnightAcademy {

    @Override
    public String getName() {
        // TODO complete me
        return "Lordran";
    }

    @Override
    public Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new LordranArmory();

        switch (type) {
            case "majestic":
                // TODO complete me
                knight = new MajesticKnight(armory);
                break;
            case "metal cluster":
                // TODO complete me
                knight = new MetalClusterKnight(armory);
                break;
            case "synthetic":
                // TODO complete me
                knight = new SyntheticKnight(armory);
                break;
        }
        // knight.prepare();
        return knight;
    }
}
