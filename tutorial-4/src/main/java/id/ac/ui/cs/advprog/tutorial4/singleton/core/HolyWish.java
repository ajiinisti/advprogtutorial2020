package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private static final HolyWish holyWish = new HolyWish();

    private String wish;

    // TODO complete me with any Singleton approach

    private HolyWish(){
    }

    public static HolyWish getInstance(){
        return holyWish;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
