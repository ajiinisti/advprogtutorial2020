package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.junit.jupiter.api.Test;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests

    @Test
    public void whenProduceKnightShouldCallAcademyRepository(){
        // academyService.produceKnight("Lordran", "majestic");
        // verify(academyRepository,times(1)).getKnightAcademyByName("Lordran").getKnight("majestic");
        AcademyService sAcademyServiceImpl = spy(academyService);
        // when(academyRepository.getKnightAcademyByName("Drangleic")).thenReturn(new DrangleicAcademy());
        when(academyRepository.getKnightAcademyByName("Lordran")).thenReturn(new LordranAcademy());
        sAcademyServiceImpl.produceKnight("Lordran", "majestic");
        verify(academyRepository,times(1)).getKnightAcademyByName("Lordran");
    } 

    @Test
    public void whenGetKnightAcademiesShouldCallAcademyRepository(){
        academyService.getKnightAcademies();
        verify(academyRepository,times(1)).getKnightAcademies();
    }

    @Test
    public void whenGetKnightShouldCallAcademyRepository(){
        // academyService.produceKnight("Lordran", "majestic");
        // academyService.getKnight();
        // verify(academyRepository,times(1)).getKnightAcademyByName("Lordran").getKnight("majestic");
        Knight knight = academyService.getKnight();
        assertThat(knight).isEqualTo(null);
    }
}
