package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Mock
    private HolyGrail holyGrail;

    @InjectMocks
    private HolyWish holyWish;
    // TODO create tests
    public void whenMakeaWishShouldCallHolyWish(){
        // HolyWish wish = HolyWish.getInstance().setWish("Halo");
        holyGrail.makeAWish("Halo");
        verify(holyGrail,times(1)).makeAWish("Halo");
    }

    public void whenGetHolyWishReturnHolyWish(){
        HolyWish wish = holyGrail.getHolyWish();
        verify(holyGrail,times(1)).getHolyWish();
    }
}
