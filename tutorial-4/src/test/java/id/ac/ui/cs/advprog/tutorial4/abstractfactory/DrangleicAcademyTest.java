package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        majesticKnight = new MajesticKnight(new DrangleicArmory());
        majesticKnight.prepare();
        metalClusterKnight = new MetalClusterKnight(new DrangleicArmory());
        metalClusterKnight.prepare();
        syntheticKnight = new SyntheticKnight(new DrangleicArmory());
        syntheticKnight.prepare();
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Majestic Knight");
        assertEquals(metalClusterKnight.getName(), "Metal Cluster Knight");
        assertEquals(syntheticKnight.getName(), "Synthetic Knight");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertTrue(majesticKnight.getArmor().getDescription()=="Armor with Metal");
        assertTrue(majesticKnight.getWeapon().getDescription()=="JackJack");
        assertNull(majesticKnight.getSkill());
        
        assertTrue(metalClusterKnight.getArmor().getDescription()=="Armor with Metal");
        assertTrue(metalClusterKnight.getSkill().getDescription()=="Make you pain in Thousand Years");
        assertNull(metalClusterKnight.getWeapon());
        
        assertTrue(syntheticKnight.getWeapon().getDescription()=="JackJack");
        assertTrue(syntheticKnight.getSkill().getDescription()=="Make you pain in Thousand Years");
        assertNull(syntheticKnight.getArmor());
    }

}
