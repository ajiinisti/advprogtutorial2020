package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private final SoulService soulService;

    public SoulController(SoulService soulService){
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        // TODO: Use service to complete me.
        return ResponseEntity.ok(soulService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        // TODO: Use service to complete me.
        return ResponseEntity.ok(soulService.register(soul));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        // TODO: Use service to complete me.
        return ResponseEntity.ok(soulService.findSoul(id).get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        // TODO: Use service to complete me.
        return ResponseEntity.ok(soulService.rewrite(soul));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        // TODO: Use service to complete me.
        soulService.erase(id);
        return ResponseEntity.ok("Soul deleted!");
    }
}

