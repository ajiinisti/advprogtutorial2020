package id.ac.ui.cs.advprog.tutorial5.core;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {
    private Class<?> soulClass;

    private Soul soul;

    @BeforeEach
    public void setUp() throws Exception {
        soulClass = Class.forName("id.ac.ui.cs.advprog.tutorial5.core.Soul");
        soul = new Soul();
        soul.setName("Aji");
        soul.setAge(20);
        soul.setGender("M");
        soul.setOccupation("Coba");
    }

    @Test
    public void testGetId(){
        assertEquals(0,soul.getId());
    }

    @Test
    public void testGetName(){
        assertEquals("Aji",soul.getName());
    }

    @Test
    public void testGetAge(){
        assertEquals(20,soul.getAge());
    }

    @Test
    public void testGetGender(){
        assertEquals("M",soul.getGender());
    }

    @Test
    public void testGetOccupation(){
        assertEquals("Coba",soul.getOccupation());
    }


    @Test
    public void testSetName(){
        soul.setName("inisti");
        assertEquals("inisti",soul.getName());
    }

    @Test
    public void testSetAge(){
        soul.setAge(10);
        assertEquals(10,soul.getAge());
    }

    @Test
    public void testSetGender(){
        soul.setGender("F");
        assertEquals("F",soul.getGender());
    }

    @Test
    public void testSetOccupation(){
        soul.setOccupation("Blabla");
        assertEquals("Blabla",soul.getOccupation());
    }
}
