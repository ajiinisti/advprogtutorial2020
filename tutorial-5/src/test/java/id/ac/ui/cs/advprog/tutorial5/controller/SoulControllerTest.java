package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private SoulRepository soulRepository;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/soul"))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindById() throws Exception{
        mockMvc.perform(get("/soul"))
        .andExpect(status().isOk());
    }

    @Test
    public void testDeleteSoul() throws Exception {
        mockMvc.perform(delete("/soul/0"))
                .andExpect(status().isOk());
    }
}
