package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me

    public String getType(){
        return "Type Shield";
    }

    public String defend(){
        return "Defend With Shield";
    }
}
