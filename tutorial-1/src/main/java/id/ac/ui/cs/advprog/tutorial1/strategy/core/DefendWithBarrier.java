package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me

    public String getType(){
        return "Type Barrier";
    }

    public String defend(){
        return "Defend With Barrier";
    }
}
