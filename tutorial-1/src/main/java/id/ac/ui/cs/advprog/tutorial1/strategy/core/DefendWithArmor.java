package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me

    public String getType(){
        return "Type Armor";
    }

    public String defend(){
        return "Defend With Armor";
    }
}
